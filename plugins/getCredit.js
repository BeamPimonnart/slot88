import { request } from '@/utils/API/getapi'

export default (context, inject) => {

    const formatNumber = (num) => {
        return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    async function getCredit() {
        const API = 'api/member/get_credit'
        const credit = await request('get', API, {}, true)
        return credit
    }
    const realtimeCredit = async() => {
        if (window.$nuxt.$auth.loggedIn === true) {
            const credit = await getCredit()
            if (credit.status === 200) {
                await getCredit()
            } else {
                this.$nuxt.$auth.logout();
                window.location.replace('/')
            }
            if (!credit) {
                window.$nuxt.$auth.logout()
                window.$nuxt.$auth.$storage.removeUniversal('_token.local')
                window.$nuxt.$auth.$storage.removeUniversal('strategy')
                window.$nuxt.$auth.$storage.setState('loggedIn', false)
                window.location.reload()
                this.$nuxt.$auth.logout();
                window.location.replace('/')
            }
            return formatNumber(credit.data.member.member_credit)
        }
    }
    inject('realtimeCredit', realtimeCredit)
}