export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'slot_vue',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'stylesheet',
                href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css',
            },
            {
                rel: 'stylesheet',
                href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css',
            },
            {
                rel: 'stylesheet',
                href: 'https://fonts.googleapis.com/icon?family=Material+Icons',
            },
            {
                rel: "stylesheet",
                href: "https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css",
            },
        ],
        script: [
            { type: 'text/javascript', src: 'https://code.jquery.com/jquery-3.5.1.min.js' },
            {
                type: 'text/javascript',
                src: 'https://code.jquery.com/jquery-3.5.1.min.js',
            },
            {
                type: 'text/javascript',
                src: 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js',
            },
            {
                type: 'text/javascript',
                src: 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js',
            },
            {
                type: 'text/javascript',
                src: 'https://kit.fontawesome.com/f59f2f6742.js',
            },
            { type: 'text/javascript', src: 'https://kit.fontawesome.com/f59f2f6742.js' },
            {
                type: 'text/javascript',
                src: 'https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js',
            },
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: ['@/assets/css/style.css', '@/assets/css/hooper.css', '@/assets/css/icon.css'],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [{ src: '~/plugins/getToken', mode: 'client' }, { src: '~/plugins/getUser', mode: 'client' }, { src: '~/plugins/play-host', mode: 'client' }, { src: '~/plugins/qr-code', mode: 'client' }, { src: '~/plugins/getCreditAff', mode: 'client' }, { src: '~/plugins/getCredit', mode: 'client' }, ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/bootstrap
        'bootstrap-vue/nuxt',
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/auth',
        'cookie-universal-nuxt',
        'vue-sweetalert2/nuxt',
    ],

    auth: {
        redirect: false,
        strategies: {
            local: {
                endpoints: {
                    login: false,
                    logout: false,
                    user: false,
                },
            },
        },
    },

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {},

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {},
    env: {
        hostname: 'Major168.net',
        api_test: 'https://web-api.major168.net',
    },
}