import { request } from "./getapi";

export async function bannerlist() {
    const url = `api/banner`;
    const banner = await request("get", url, {}, false);

    if (banner.status === 200) {
        return banner.data.banner;
    } else {
        return;
    }
}
export async function GameListslotIndex() {
    const url = `api/game/slo`
    const listslot = await request('get', url, {}, false)

    if (listslot.status === 200) {
        return listslot.data.games;
    } else {
        return;
    }
}

export async function GameListslot() {
    const url = `api/game/slo`
    const listslot = await request('get', url, {}, false)

    if (listslot.status === 200) {
        return listslot
    } else {
        return;
    }
}

export async function slotsnavbar() {
    const url = `api/game/slo/main`
    const listslotsnavbar = await request('get', url, {}, false)

    if (listslotsnavbar.status === 200) {
        return listslotsnavbar
    } else {
        return;
    }
}

export async function getContact() {
    const url = `api/website/contact`
    const listcontact = await request('get', url, {}, false)

    if (listcontact.status === 200) {
        return listcontact.data.data;
    } else {
        return;
    }


}
export async function BonusListing() {
    const url = `api/bonus`
    const promo = await request('get', url, {}, false)

    if (promo.status === 200) {
        return promo.data.bonus;
    } else {
        return;
    }
}
export async function WithdrawInformation() {
    const url = `api/member/withdraw`
    return await request('get', url, {}, true)
}
export async function withdrawSubmit(withdrawAmount) {
    const url = `api/member/withdraw/submit`
    return await request('post', url, { withdraw_amount: withdrawAmount }, true)
}
export async function withdrawhistory() {
    const url = `api/member/withdraw/history`
    return await request('get', url, {}, true)
}

export async function affsummary() {
    const url = `api/member/affiliate/summary_report`
    const affsum = await request('get', url, {}, true)
    return affsum;
}

export async function affreferrer() {
    const url = `api/member/affiliate/referrer_lists`
    const affref = await request('get', url, {}, true)
    return affref;
}

export async function withdrawCancel(withdrawIransactionId) {
    const url = `api/member/withdraw/cancel`
    return await request('post', url, { withdraw_transaction_id: withdrawIransactionId }, true)
}

export async function deposit() {
    const url = `api/member/deposit`
    return await request('get', url, {}, true)
}

export async function deposithistory() {
    const url = `api/member/deposit/history`
    return await request('get', url, {}, true)
}

export async function changepassword(oldPassword, newPassword, newRPassword) {
    const url = `api/member/change_password`
    return await request('post', url, { old_password: oldPassword, new_password: newPassword, new_r_password: newRPassword }, true)
}

export async function bonusreceive(bonusId) {
    const url = `api/bonus/receive`
    return await request('post', url, { bonus_id: bonusId }, true)
}

export const getGaChaHistory = async() => {
    const url = `api/gacha/history`
    return await request('get', url, {}, true)
}

export const getgift = async() => {
    const url = `api/gacha/exchange`
    const web = await request('get', url, {}, true)
    return await web.data
}

export async function postgift(id) {
    const url = `api/gacha/exchange`
    return await request('post', url, { id }, true)
}