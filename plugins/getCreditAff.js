import { request } from '@/utils/API/getapi'

export default (context, inject) => {

    const formatNumber = (num) => {
        return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    async function getCreditAff() {
        const API = 'api/member/affiliate/summary_report'
        const creditAff = await request('get', API, {}, true)
        return creditAff
    }
    const realtimeCreditAff = async() => {
        if (window.$nuxt.$auth.loggedIn === true) {
            const creditAff = await getCreditAff()
            if (creditAff.status === 200) {
                await getCreditAff()
            } else {
                this.$nuxt.$auth.logout();
                window.location.replace('/')
            }
            if (!creditAff) {
                window.$nuxt.$auth.logout()
                window.$nuxt.$auth.$storage.removeUniversal('_token.local')
                window.$nuxt.$auth.$storage.removeUniversal('strategy')
                window.$nuxt.$auth.$storage.setState('loggedIn', false)
                window.location.reload()
                this.$nuxt.$auth.logout();
                window.location.replace('/')
            }
            return formatNumber(creditAff.data.datas[0].total_amount)
        }
    }
    inject('realtimeCreditAff', realtimeCreditAff)
}