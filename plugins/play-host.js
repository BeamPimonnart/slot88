export default (context, inject) => {
    const playHost = () => {
        let host = location.hostname.replace('wallet.', '')
        if (host === 'localhost') {
            host = process.env.hostname
        }
        return `https://web-play.${host}/`
    }
    inject('playHost', playHost)
}