import { request } from './getapi'
export async function step1(username) {
  const url = `api/register/check_user`
  return await request('post', url, { username }, false)
}

export async function step2(memberBank, memberBankAccountNumber, memberFirstname, memberLastname, memberFirstnameEn, memberLastnameEn) {
  const url = `api/register/check_bankinfo`
  return await request(
    'post',
    url,
    {
      member_bank: memberBank,
      member_bank_account_number: memberBankAccountNumber,
      member_firstname: memberFirstname,
      member_lastname: memberLastname,
      member_firstname_en: memberFirstnameEn,
      member_lastname_en: memberLastnameEn,
    },
    false
  )
}

export async function step3(
  memberBank,
  memberBankAccountNumber,
  memberFirstname,
  memberLastname,
  memberFirstnameEn,
  memberLastnameEn,
  memberUsername,
  memberPassword,
  registerRecommend,
  affiliateCode,
  memberEmail,
  memberLineid,
) {
  const url = `api/register/submit`
  return await request(
    'post',
    url,
    {
      member_bank: memberBank,
      member_bank_account_number: memberBankAccountNumber,
      member_firstname: memberFirstname,
      member_lastname: memberLastname,
      member_firstname_en: memberFirstnameEn,
      member_lastname_en: memberLastnameEn,
      member_username: memberUsername,
      member_password: memberPassword,
      register_recommend: registerRecommend,
      affiliate_code: affiliateCode,
      member_email: memberEmail,
      member_lineid: memberLineid
    },
    false
  )
}
