import { request } from './getapi'

const API_URL = process.env.API_URL

const API_LOGIN = 'api/login'
const API_INFO = 'api/member/info'
const API_GIVE_PROMOTION = 'api/bonus/receive'
const API_PASSWORD = 'api/member/change_password'

export const login = async(username, password) => {
    const callback = await request('post', API_LOGIN, { username, password }, false)
    if (callback.data.status === true) {
        window.$nuxt.$auth.setToken('local', 'Bearer ' + callback.data.token)
        window.$nuxt.$auth.setUser(await getInfo().then(res => {
            return res.data.member
        }))
    }
    return callback.data
}

export const getInfo = async() => {
    const info = await request('get', API_INFO, {}, true)
    if (info.data.status === true) {
        return info
    } else {
        window.$nuxt.$auth.logout()
        window.location.replace('/member/login')
        window.location.reload()
    }
}


export const givePromotion = async(id) => {
    const result = await request('post', API_GIVE_PROMOTION, { bonus_id: id }, true)
    return result.data
}

export const changePassword = async(op, np, npc) => {
    const result = await request('post', API_PASSWORD, { old_password: op, new_password: np, new_r_password: npc }, true)
    return result.data
}