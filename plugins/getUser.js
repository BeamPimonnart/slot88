import { getInfo } from "@/utils/API/apiuser";
export default (context, inject) => {
    const user = async() => {
        const info = await getInfo()
        if (!info) {
            window.$nuxt.$auth.logout()
            window.$nuxt.$auth.$storage.removeUniversal('_token.local')
            window.$nuxt.$auth.$storage.removeUniversal('strategy')
            window.$nuxt.$auth.$storage.setState('loggedIn', false)
            window.location.reload()
            this.$nuxt.$auth.logout()
            window.location.replace('/')
        }
        return info.data.member
    }
    inject('user', user)
}