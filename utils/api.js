import axios from 'axios'

const getToken = () => {
    if (process.server) {
        return
    }
    if (window.$nuxt) {
        return window.$nuxt.$auth.getToken('local')
    }
}

export const request = async(method, endpoint, data, auth = false) => {
    const headers = {}
    if (auth) {
        headers.Authorization = getToken()
    }
    let url = ''
    if (process.client) {
        url = location.hostname.replace('wallet.', '')
        url = url.replace('v1.', '')
        url = url.replace('v2.', '')
        if (url === 'localhost') {
            url = `${process.env.api_test}/${endpoint}`
        } else {
            url = `https://web-api.${url}/${endpoint}`
        }
    }
    try {
        const result = await axios({
            method,
            url,
            data,
            headers,
        }).catch((err) => {
            let error = new Error()
            error = {...error, status: err.response.status, response: err.response }
            throw error
        })
        return result
    } catch (err) {
        console.log(err)
            // if (err.status === 403 || err.status === 401) {
            //   return { data: { status: false } }
            // }
        return err.response
    }
}