import axios from 'axios'
const getToken = function() {
    if (process.server) {
        return
    }
    if (window.$nuxt) {
        return window.$nuxt.$auth.getToken('local')
    }
}

export async function request(method, endpoint, data, auth = false) {
    const headers = {}
    if (auth) {
        headers.Authorization = getToken()
    }
    let url = ''
    if (process.client) {
        url = location.hostname.replace('wallet.', '')
        url = url.replace('v1.', '')
        url = url.replace('v2.', '')
        if (url === 'localhost') {
            url = `${process.env.api_test}/${endpoint}`
        } else {
            url = `https://web-api.${url}/${endpoint}`
        }
    }
    try {
        const response = await axios({
            method,
            url,
            data,
            headers,
        })
        return response
    } catch (err) {
        if (err.status !== 200) {
            return { data: { status: false } }
        }
        return err.response
    }
}