import { request } from "./api";

let HOSTNAME = "";
if (process.client) {
    if (window.location.host !== "localhost:3000") {
        HOSTNAME = window.location.host;
        HOSTNAME = HOSTNAME.split(".");
        if (HOSTNAME[0] === "www") {
            HOSTNAME = `https://web-api.${HOSTNAME[1]}.${HOSTNAME[2]}/`;
        } else {
            HOSTNAME = `https://web-api.${window.location.host}/`;
        }
    } else {
        HOSTNAME = process.env.apiUrl;
    }
}

export async function login(username, password) {
    const login = await request(
        "post",
        `api/login`, { username, password },
        false
    );
    if (login.data.status === true) {
        await window.$nuxt.$auth.setToken("local", "Bearer " + login.data.token);
        const info = await request("get", `api/member/info`, {}, true);
        if (info.data.status === false) {
            return info.data;
        }
    }
    return login.data;
}

export async function info() {
    const info = await request("get", `api/member/info`, {}, true);
    return info.data.member;
}