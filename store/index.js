export const state = () => ({
    isLoggedIn: true,
    credit: 0,
    creditAff: 0,
    user: [],
    token: '',
})

export const mutations = {
    login(state) {
        state.isLoggedIn = true
    },
    logout(state) {
        state.isLoggedIn = false
    },
    setCredit(state, value) {
        state.credit = value
    },
    setCreditAff(state, value) {
        state.creditAff = value
    },
    setUser(state, value) {
        state.user = value
    },
    setToken(state, value) {
        state.token = value
    }
}